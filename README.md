# t10

![captura del script](pantallazo.png)

Script en Bash que muestra el top 10 de los procesos que más consumen de RAM y CPU.

La idea principal del script está sacada de un script que Fanta compartió en su [blog](https://56k.es/fanta/t10-top-10-de-procesos-que-mas-ram-y-cpu-consumen-en-linux/) 

A partir de ese código y por diversión y para aprender, le añadí colorines y formatee la salida para que se viera en columnas, una al lado de la otra.

Si la consola donde se ejecuta no tiene el ancho para que se vea la información en dos columnas, la salida no se mostrará en una única columna.

_Enlace al artículo en [mi blog](https://victorhckinthefreeworld.com/2022/07/11/script-en-bash-que-muestra-el-top10-de-procesos-que-consumen-ram-y-cpu/)_

## Instalación

* Descarga el script ejecutando: `wget https://codeberg.org/victorhck/top10/raw/branch/main/t10` 
* Dale permisos de ejecución: `chmod +x t10`
* Ejecútalo mediante: `./t10`
* Si quieres puedes mover el script a una ruta de tu `path` para poder ejecutarlo desde cualquier ubicación

